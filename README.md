# BubbleUPnP Server

This creates a Docker container running [BubbleUPnP Server](https://bubblesoftapps.com/bubbleupnpserver/). It should support hardware accelerated transcoding with Nvidia GPUs, however I could not test this because I own an AMD GPU.

## Build and run

```
sudo docker build -t bubbleupnpserver .
sudo docker run --rm --name=bubbleupnpserver --net=host bubbleupnpserver
```

Now open http://localhost:58050 in your browser and check the settings.

There is also an alternative Dockerfile enclosed using Arch Linux as base image, if you prefer bleeding edge software packages.
