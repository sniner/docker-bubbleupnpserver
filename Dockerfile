FROM debian:stretch-slim
LABEL maintainer="Stefan Schönberger <mail@sniner.net>"

RUN set -ex && \
    sed -i '/^deb / s/$/ contrib non-free/' /etc/apt/sources.list && \
    mkdir -p /usr/share/man/man1 && \
    apt-get update && \
    apt-get install --no-install-recommends --no-install-suggests -y \
        openjdk-8-jre-headless unzip wget sox nvidia-driver && \
    rm -rf /var/lib/apt/lists/* && \
    \
    mkdir -p /opt/bubbleupnpserver && \
    cd /opt/bubbleupnpserver && \
    wget http://www.bubblesoftapps.com/bubbleupnpserver/BubbleUPnPServer-distrib.zip && \
    unzip BubbleUPnPServer-distrib.zip && \
    wget https://bubblesoftapps.com/bubbleupnpserver/core/ffmpeg_linux.zip && \
    unzip ffmpeg_linux.zip && \
    chmod +x launch.sh && \
    rm BubbleUPnPServer-distrib.zip ffmpeg_linux.zip launch.bat

EXPOSE 58050/tcp 58051/tcp 1900/udp

ENTRYPOINT ["/opt/bubbleupnpserver/launch.sh"]
